package labs.lab0;
//Lillian Hartwig, CS201-01, 1/19/21, Square
public class Square {

	public static void main(String[] args) {
		// create a square
		// char c = '+';
		// int n = 6;	// size of square
		// int i, j; // dummy variables
		// for( i = 1; i <= n; i++){
		//	for(j = 1; j <= n; j++){
		//		System.out.print(c);
		//	}
		//	System.out.print("\n");
		//}
		
		char c = '@';
		int n = 5;		// size of square
		int i, j;		// dummy variables
		for( i = 1; i <= n; i++)
		{
			for(j = 1; j <= n; j++)
			{
				System.out.print(c);
			}
			System.out.print("\n");
		}
		// From my code the desired result was obtained
	}
}
