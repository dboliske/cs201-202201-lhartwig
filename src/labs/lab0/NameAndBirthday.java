package labs.lab0;
//Lillian Hartwig, CS201-01, 1/19/21, NameAndBirthday
import java.util.Scanner;

public class NameAndBirthday {

	public static void main(String[] args) {
		// print your name and birthday in the specific format
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt for name
		System.out.print("Name: ");
		String name = input.nextLine();
		
		// prompt for birthday
		System.out.print("Month (word): ");
		String month = input.nextLine();
		System.out.print("Day: ");
		String day = input.nextLine();
		System.out.print("Year: ");
		String year = input.nextLine();
		
		// print out character
		System.out.println("My name is " + name + " and my birthdate is " + month + " " + day + ", " + year + ".");
		
		// close scanner
		input.close();
	}
}