package labs.lab7;

public class ExerciseTwo {
	// Lillian Hartwig, CS201-01, 4/3/22, ExerciseTwo
	
	public static String[] insertionSort(String[] array) {
		for (int j=1; j<array.length; j++) {
			int i = j;
			while (i > 0 && array[i].compareTo(array[i-1]) < 0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		String[] array = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		array = insertionSort(array);
		
		for (String a:array) {
			System.out.print(a + " ");
		}
	}
}