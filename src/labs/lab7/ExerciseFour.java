package labs.lab7;

import java.util.Scanner;

public class ExerciseFour {
	// Lillian Hartwig, CS201-01, 4/3/22, ExerciseFour
	
	public static int binarySearch(String[] array, String value) {
		int start = 0;
		int end = array.length;
		int pos = -1;
		boolean found = false;
		while (!found && start != end) {
			int middle = (start + end) / 2;
			if (array[middle].equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (array[middle].compareToIgnoreCase(value) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		
		return pos;
	}

	public static void main(String[] args) {
		String[] array =  {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		String value = input.nextLine();
		int index = binarySearch(array, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(value + " found at index " + index + ".");
		}

		input.close();
	}
}