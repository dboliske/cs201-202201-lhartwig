package labs.lab7;

public class ExerciseThree {
	// Lillian Hartwig, CS201-01, 4/3/22, ExerciseThree
	
	public static String[] string(Double[] array) {
		String[] arrayString = new String[array.length]; // switch to String
		for (int i = 0; i < arrayString.length; i++) {
			arrayString[i] = String.valueOf(array[i]);  
		}
		
		return arrayString;
	}
	
	public static String[] selectionSort(String[] array) {
		for (int i=0; i<array.length - 1; i++) {
			int min = i;
			for (int j=i+1; j<array.length; j++) {
				if (array[j].compareTo(array[min]) < 0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}
	
	public static Double[] stringToDouble(String[] array) {
		Double[] arrayDouble = new Double[array.length]; // switch String back to Double
		for (int i = 0; i < array.length; i++) {
			arrayDouble[i] = Double.parseDouble(array[i]);
		}
		
		return arrayDouble;
	}

	public static void main(String[] args) {
		Double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		String[] arrayString = new String[array.length];
		arrayString = string(array); // swtich to String
		
		arrayString = selectionSort(arrayString); // sort by selection
		
		array = stringToDouble(arrayString); // back to Double
		
		for (Double a:array) { // print sorted array
			System.out.print(a + " ");
		}
	}
}