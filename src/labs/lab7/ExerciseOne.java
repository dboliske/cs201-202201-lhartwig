package labs.lab7;

public class ExerciseOne {
	// Lillian Hartwig, CS201-01, 4/3/22, ExerciseOne
	
	public static String[] string(int[] array) {
		String[] arrayString = new String[array.length]; // switch to String
		for (int i = 0; i < arrayString.length; i++) {
			arrayString[i] = String.valueOf(array[i]);  
		}
		
		return arrayString;
	}
	
	public static String[] bubbleSort(String[] array) {
		
		boolean done = false;
		
		do {
			done = true;
			for (int i=0; i<array.length - 1; i++) { // bubble sort
				if (array[i+1].compareTo(array[i]) < 0) {
					String temp = array[i+1];
					array[i+1] = array[i];
					array[i] = temp;
					done = false;
				}
			}
		} while (!done);
		
		return array;
	}
	
	public static int[] stringToInt(String[] array) {
		int[] arrayInt = new int[array.length]; // switch String back to integer
		for (int i = 0; i < array.length; i++) {
			arrayInt[i] = Integer.parseInt(array[i]);
		}
		
		return arrayInt;
	}

	public static void main(String[] args) {
		int[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		String[] arrayString = new String[array.length];
		arrayString = string(array); // swtich to String
		
		arrayString = bubbleSort(arrayString); // sort array
		
		array = stringToInt(arrayString); // back to int
		
		for (int a:array) { // print sorted array
			System.out.print(a + " ");
		}
	}
}