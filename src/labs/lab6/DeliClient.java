package labs.lab6;

import java.util.Scanner;

public class DeliClient extends Deli {
	// Lillian Hartwig, CS201-01, 3/19/22, DeliClient
	
	public static int addCustomer(String[] list, int size, String customer) {
		// resize if necessary
		if (size == list.length) {
			String[] temp = new String[list.length  * 2];
			for (int i =  0; i < list.length; i++) {
				temp[i] = list[i];
			}
			list = temp;
			temp = null;
			list[size] = customer;
		} else { // no resize necessary; add in new customer
			list[size] = customer;
		}
		
		size++;
		return size; // customer position
	}
	
	public static String helpCustomer(String[] list, int size) {
		String temp = list[0];
		for (int i = 1; i < size; i++) {
			list[i - 1] = list[i]; // shift list of remaining customers 
		}
		size--;
		return temp; // next customer
	}

	public static void main(String[] args) {
		Deli deli = new Deli(); // creates a deli queue
		Scanner input = new Scanner(System.in); // create scanner
		
		boolean go = true;
		do { 
			System.out.println("Menu"); // print out menu
			System.out.println("1. Add Customer");
			System.out.println("2. Help Customer");
			System.out.println("3. Exit");
			System.out.print("Enter an option: "); // assuming user will only enter 1, 2, or 3
			String choice = input.nextLine();
			
			switch(choice) {
			case "1":
				// add customer
				System.out.print("Enter customer name: ");
				String customer = input.nextLine();
				int size = addCustomer(deli.getList(), deli.getSize(), customer);
				System.out.println("Customers queued: " + size);
				break;
			case "2":
				// help customer
				String next = helpCustomer(deli.getList(), deli.getSize());
				System.out.println("Next customer to be helped: " + next);
				break;
			case "3":
				// exit
				go = false;
				break;
			default:
				break;
			}
		} while (go);
		input.close(); // close scanner
		System.out.println("Goodbye."); // show user that program has ended
	}
}