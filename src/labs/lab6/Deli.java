package labs.lab6;

public class Deli {
	// Lillian Hartwig, CS201-01, 3/19/22, Deli

	private int size;
	private String[] list;
	private String data;
	
	public Deli() {
		size = 0;
		list = new String[15];
	}
	
	public int getSize() {
		return size;
	}
	
	public String[] getList() {
		return list;
	}
	
	public String getData() {
		return data;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setList(String[] list) {
		this.list = list;
	}

	public void setData(String data) {
		this.data = data;
	}
}