package labs.lab3;


public class MinimumValue {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 2/6/22, MinimumValue
		// find the min value and print it
		int[] array = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		int min = array[0]; // initial minimum value
		for(int i = 0; i<array.length;) { // test each value of the array
			if(min == array[i]) { // sort between even to, less than, or greater than
				i++;}
				else if(min < array[i]) {
					i++;}
				else if(min > array[i]) {
					min = array[i];
					i++;}
		}
		System.out.println("The minimum value is " + min); // print out the minumum  value
	}
}