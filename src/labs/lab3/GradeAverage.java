package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class GradeAverage {

	public static void main(String[] args) throws IOException {
		// Lillian Hartwig, CS201-01, 2/6/22, GradeAverage
		// read in file and compute average grade
		
		File f = new File("src/labs.lab3/grades.csv");
		Scanner input = new Scanner(f);
		
		int grades[] = new int[15]; // create an array large enough
		
		int count = 0;
		while(input.hasNextLine()) {
			String line = input.nextLine(); // save data from file to array
			String[] values = line.split(",");
			grades[count] = Integer.parseInt(values[1]);
			count++; // use count to know how many grades there are
		}
		
		input.close(); // close scanner
		
		int average = 0;
		for(int i = 0; i<count; i++) {
			average = average + grades[i]; // add grades together 
		}
		
		System.out.println("The average grade is " + (average/count)); // print average
	}
}