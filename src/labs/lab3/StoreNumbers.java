package labs.lab3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class StoreNumbers {

	public static void main(String[] args) throws IOException {
		// Lillian Hartwig, CS201-01, 2/6/22, StoreNumbers
		// store numbers and save to file
		Scanner input = new Scanner(System.in); // create scanner
		
		boolean done = false; // flag variable
		int numbers[] = new int[50]; // create empty array
		String test = ""; // create empty string to detect when user is done
		int count = 0;
		do {
			try {
			System.out.print("Enter a number or say done when complete: ");
			numbers[count] = input.nextInt(); // saves integers to the array
			count++;
			done = false;
			} catch (Exception e) {
				test = input.nextLine(); // non integers go to the test variable
				System.out.println(test);
				if ("Done".equalsIgnoreCase(test)) { // if test is done then program moves on
					System.out.println("User entered done");
					done = true;
				} else {
					System.out.println("Invalid input."); // if not, user is told they did something wrong
					done = false;
				}
			}
		} while(!done);
		
		String name = "";
		System.out.print("Enter a file name ending in .txt: ");
		name = input.nextLine(); // prompts user for file name and stores string
		File file = new File(name); // create the file
		file.createNewFile();
		try {
			FileWriter f = new FileWriter(file);
			for (int i=0; i < count; i++) {
				f.write(numbers[i] + "\n"); // write to the fil
			}
			f.flush();
			f.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		input.close();
		System.out.println("Finished Writing"); // let user know that the file is finished
	}
}