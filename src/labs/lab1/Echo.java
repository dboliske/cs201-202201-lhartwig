package labs.lab1;

import java.util.Scanner;

public class Echo {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/21/21, Echo
		// prompt user for name and echo name to console
		
		Scanner input = new Scanner(System.in); // create scanner for name
		
		System.out.print("Enter your name: "); // prompt for name
		String name = input.nextLine(); // save name
		
		System.out.print("Your name is " + name); // echo name
		
		input.close(); // close scanner
	}
}