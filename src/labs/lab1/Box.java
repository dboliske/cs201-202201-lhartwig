package labs.lab1;

import java.util.Scanner;

public class Box {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/21/21, Box
		// calculate the surface area of a box to know how much wood is needed
		
		Scanner input = new Scanner(System.in); // create scanner
		System.out.println("Enter the dimensions of the box in inches: "); // prompt for dimensions
		
		System.out.print("Length: ");
		float length = Float.parseFloat(input.nextLine()); // store as floatint-point
		
		System.out.print("Width: ");
		float width = Float.parseFloat(input.nextLine()); // store as floatint-point
		
		System.out.print("Height: ");
		float height = Float.parseFloat(input.nextLine()); // store as floatint-point
		
		float SA = (2*length*width + 2*width*height + 2*height*length); // calculate surface area
		
		System.out.print("To build a box of dimensions " + length + " by " + width + " by " + height + ", " + SA/144 + " square feet of wood is needed.");
		// output message and surface area in sq. ft
		
		input.close(); // close scanner
		
		// Test
		// 	Inputs: 2, 2, 2; Expected output: 0.167; Actual output: 0.16666667
		//	Inputs: 5, 10, 15; Expected output: 3.819; Actual output: 3.8194444
		// 	Inputs: 20, 20, 25; Expected output: 19.45; Actual output: 19.444445
	}
}