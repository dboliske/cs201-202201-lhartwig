package labs.lab1;

public class DoArithmetic {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/21/21, DoArithmetic
		// output the results of certain calculations
		
		System.out.println(20 - 49); // my age subtracted by my fathers
		System.out.println(2001*2); // my birth year multiplied by 2
		int height = 68; // my height in inches
		System.out.println(height*2.54); // my height in centimeters
		System.out.println(height/12 + " feet and " + height%12 + " inches"); // my height in feet and inches
	}
}