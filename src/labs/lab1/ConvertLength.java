package labs.lab1;

import java.util.Scanner;

public class ConvertLength {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/21/21, ConvertLength
		// convert inches to centimeters
		
		Scanner input = new Scanner(System.in); // create scanner
		
		System.out.print("Enter length in inches: "); // prompt for inches
		float inches = Float.parseFloat(input.nextLine()); // store input
		
		System.out.print("Length in centimeters: " + (inches*2.54)); // convert to centimeters and print
		
		input.close(); // close scanner
		
		// Test
		// 	Inputs: 1; Expected output: 2.54; Actual output: 2.54
		//	Inputs: 12; Expected output: 30.48; Actual output: 30.48
		//	Inputs: 60; Expected output: 152.4; Actual output: 152.4
	}
}