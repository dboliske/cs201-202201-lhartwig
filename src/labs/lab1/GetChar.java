package labs.lab1;

import java.util.Scanner;

public class GetChar {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/21/21, GetChar
		// getting a character from user input
		
		Scanner  input = new Scanner(System.in); // create scanner
		
		System.out.print("Enter your first name: ");
		char initial = input.nextLine().charAt(0);
		
		System.out.print("Your first initial is " + initial);
		
		input.close();
	}
}