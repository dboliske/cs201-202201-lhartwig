package labs.lab1;

import java.util.Scanner;

public class ConvertTemp {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/21/21, ConvertTemp
		// convert Fahrenheit to Celsius and Celsius to Fahrenheit
		
		Scanner input = new Scanner(System.in); // create scanner
		
		System.out.print("Enter a temperature in Fahrenheit: "); // prompt for Fahrenheit
		float fahrenheit = Float.parseFloat(input.nextLine()); // store as floatint-point
		float convertCelsius = (((fahrenheit - 32) * 5) / 9); // convert to celsius
		System.out.println("Temperature in Celsius: " + convertCelsius); // print conversion
		
		System.out.print("Enter a temperature in Celsius: "); // prompt for Celsius
		float celsius = Float.parseFloat(input.nextLine()); // store as floatint-point
		float convertFahrenheit = (((celsius * 9) / 5) + 32); // convert to fahrenheit
		System.out.println("Temperature in Fahrenheit: " + convertFahrenheit); // print conversion
		
		input.close();
		
		// Fahrenheit to Celsius Test:
		// 	Input: 32; Expected output: 0; Actual output: 0.0
		// 	Input: 212; Expected output: 100; Actual output: 100.0
		// 	Input: 5; Expected output: -15; Actual output: -15.0
		
		// Celsius to Fahrenheit Test:
		// 	Input: 0; Expected output: 32; Actual output: 32.0
		// 	Input: 100; Expected output: 212; Actual output: 212.0
		// 	Input: 5; Expected output: 41; Actual output: 41.0
	}
}