package labs.lab2;
import java.util.Scanner;

public class GradeAverage {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/27/22, GradeAverage
		// compute grade average
		int i; // declare i
		int grade[]; // declare grade as an array
		float average = 0; // store average as floatint-point starting at 0
		
		Scanner input = new Scanner(System.in); // create scanner
		
		System.out.print("Enter number of grades: "); // prompt for number of students
		int n = Integer.parseInt(input.nextLine()); // save as integer
		
		grade = new int[n]; // allocate memory for array of n size
		
		for (i=0; i<n; i++) {
			System.out.print("Enter grade: "); // prompt for grades
			grade[i] = Integer.parseInt(input.nextLine()); // store as integers
			average = average + grade[i]; // add to average with each repitition of the loop
			input.close();
		}
		
		System.out.print("Average grade: " + (average/n)); // print the average for the user
	}
}