package labs.lab2;
import java.util.Scanner;

public class Square {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/27/22, Square
		// create a square based on user input
		Scanner input = new Scanner(System.in); // create scanner
		
		System.out.print("Size: "); // prompt user
		int size = Integer.parseInt(input.nextLine()); // store size as integer
		
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size; col++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		input.close(); // close scanner
	}
}