package labs.lab2;
import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 1/27/22, Menu
		// display a menu of choices to the user
		Scanner input = new Scanner(System.in); // create scanner
		boolean running = true; // flag variable
		
		while(running) {
			// print user menu
			System.out.println("a. Say 'Hello'");
			System.out.println("b. Do Addition");
			System.out.println("c. Do Multiplication");
			System.out.println("d. Exit Program");
			System.out.print("Choice: ");
			String in = input.nextLine();
			
			// handle user menu option
			switch(in) {
				case "a":
					System.out.println("Hello");
					break;
				case "b":
					System.out.print("Enter value 'a': ");
					int a = Integer.parseInt(input.nextLine());
					System.out.print("Enter value 'b': ");
					int b = Integer.parseInt(input.nextLine());
					System.out.println("a + b = " + (a + b));
					break;
				case "c":
					System.out.print("Enter value 'c': ");
					int c = Integer.parseInt(input.nextLine());
					System.out.print("Enter value 'd': ");
					int d = Integer.parseInt(input.nextLine());
					System.out.println("c * d = " + (c * d));
					break;
				case "d":
					running = false; // change flag variable
					break;
				default:
					System.out.println("I'm sorry, but that's not an option.");
			}
		}
		input.close(); // close scanner
		System.out.println("Goodbye.");
	}
}