package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {

	public static CTAStation[] readFile(String filename) {
		// Lillian Hartwig, CS201-01, 3/6/22, CTAStopApp
		CTAStation[] stations = new CTAStation[35]; // create array
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			// end-of-file loop
			GeoLocation l = null;
			CTAStation c = null;
			while (input.hasNextLine()) { // read in file
				try {
					for (int i = 0; i < stations.length; i++) {
						String line = input.nextLine();
						String[] values = line.split(",");
						l = new GeoLocation(Double.parseDouble(values[1]),Double.parseDouble(values[2]));
						c = new CTAStation(values[0],
								Double.parseDouble(values[1]),
								Double.parseDouble(values[2]),
								l.toString(),
								Boolean.parseBoolean(values[4]),
								Boolean.parseBoolean(values[5]));
						stations[i] = c;
					}
				} catch (Exception e) {
					// no next line. do nothing
				}
				}
			input.close();
		} catch (FileNotFoundException fnf) {
			System.out.println("File not found.");
		} catch(Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		
		return stations;
	}
	
	public static CTAStation[] menu(CTAStation[] stations) {
		boolean done = false;
		
		do {
			Scanner input = new Scanner(System.in);
			System.out.println("Select an option"); // menu options
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // display station names
					displayStationNames(stations);
					break;
				case "2": // display w or w/o wheelchair
					displayByWheelchair(stations, input);
					break;
				case "3": // display nearest station
					displayNearest(stations);
					break;
				case "4": // Exit
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I didn't understand that.");
			}
			
			input.close();
		} while (!done);
		
		return stations;
	}
	
	public static void displayStationNames(CTAStation[] stations) {
		for (int i=0; i<stations.length; i++) {
			System.out.println(stations[i].getName()); // print station names
		}
	}
	
	public static void displayByWheelchair(CTAStation[] stations, Scanner input) {
		boolean accessible = yesNoPrompt("Wheelchair accessible?", input);
		for (int i = 0; i < stations.length; i++) {
			if (accessible) {
				if (accessible == stations[i].hasWheelchair()) {
					System.out.println(stations[i].getName()); // print w/ wheelchairs
				}
			} else if (!accessible) {
				if (accessible == stations[i].hasWheelchair()) {
					System.out.println(stations[i].getName()); // print w/o wheelchairs
				}
			}
		}
	}
	
	public static void displayNearest(CTAStation[] stations) {
		Scanner input = new Scanner(System.in); // create scanner
		System.out.print("Enter latitude: "); // user input for location
		double lat = Double.parseDouble(input.nextLine());
		System.out.print("Enter longitude: ");
		double lng = Double.parseDouble(input.nextLine());
		input.close(); // close scanner
		CTAStation closest = null; // storage for closest station
		double distance = 500.0;
		double temp = 0; // temporary storage for distance from station
		for (int i  = 0; i < stations.length; i++) { // goes through each station pulling out the closest or moving to the next
			temp = stations[i].calcDistance(lat, lng);
			if (temp < distance) {
				distance = temp;
				closest = stations[i];
				temp = 0;
			} else {
				temp = 0;
			}
		}
		System.out.println("The closest station is " + closest.toString()); // prints closest station string
	}
	
	public static boolean yesNoPrompt(String prompt, Scanner input) { // yesNoPrompt from in-class lectures
		System.out.print(prompt + " (y/n): ");
		boolean result = false;
		String yn = input.nextLine();
		switch (yn.toLowerCase()) {
			case "y":
			case "yes":
				result = true;
				break;
			case "n":
			case "no":
				result = false;
				break;
			default:
				System.out.println("That is not yes or no. Using no.");
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		// read file
		Scanner input = new Scanner(System.in); // create scanner
		System.out.print("Load file: "); // prompt user for file
		String filename = input.nextLine(); // store
		input.close(); // close scanner
		CTAStation[] stations = readFile(filename); // read in file
		
		// menu
		stations = menu(stations); // run the menu
		
		System.out.println("Goodbye!"); // let user know program has ended
	}
}