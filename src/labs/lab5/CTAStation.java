package labs.lab5;

public class CTAStation extends GeoLocation {
	// Lillian Hartwig, CS201-01, 3/4/22, CTAStation
	
	private String name; // attributes
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() { // default constructor
		super();
		name = "35th-Bronzeville-IIT";
		setName(name);
		location = super.toString();
		setLocation(location);
		wheelchair = true;
		setWheelchair(wheelchair);
		open = true;
		setOpen(open);
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng); // non-default constructor
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}

	public String getName() { // accessor
		return name;
	}

	public void setName(String name) { // mutator
		this.name = name;
	}

	public String getLocation() { // accessor
		return location;
	}

	public void setLocation(String location) { // mutator
		this.location = location;
	}

	public boolean hasWheelchair() { // accessor
		if (wheelchair = true) {
			return true;
		} else {
			return false;
		}
	}

	public void setWheelchair(boolean wheelchair) { // mutator
		this.wheelchair = wheelchair;
	}

	public boolean isOpen() { // accessor
		if (open = true) {
			return true;
		} else {
			return false;
		}
	}

	public void setOpen(boolean open) { // mutator
		this.open = open;
	}

	@Override
	public String toString() { // preset string
		return "CTAStation [name: " + name + ", location: " + location + ", wheelchair: " + hasWheelchair()
				+ ", open: " + isOpen() + "]";
	}


	@Override
	public boolean equals(Object obj) { // checks if two CTAStations are equal
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof CTAStation)) {
			return false;
		}
		
		CTAStation c = (CTAStation)obj;
		if (!(this.name == c.getName())) {
			return false;
		} else if (!(this.location == c.getLocation())) {
			return false;
		} else if (!(this.wheelchair == c.hasWheelchair())) {
			return false;
		} else if (!(this.open == c.isOpen())) {
			return false;
		}
		
		return true;
	}
}