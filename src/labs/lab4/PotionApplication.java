package labs.lab4;

public class PotionApplication {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 2/16/22, PotionApplication
		// create two instances and print w/ toString
		
		Potion p1 = new Potion(); // default constructor
		Potion p2 = new Potion("Invisibility", 8); // non-default constructor
		
		System.out.println(p1.toString()); // print each instance w/ toString
		System.out.println(p2.toString());
	}
}