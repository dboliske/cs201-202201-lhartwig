package labs.lab4;

public class PhoneNumberApplication {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 2/16/22, PhoneNumberApplication
		// create two instances and print w/ toString
		
		PhoneNumber pn1 = new PhoneNumber(); // default constructor
		PhoneNumber pn2 = new PhoneNumber("1", "312", "7654321"); // non-default constructor
		
		System.out.println(pn1.toString()); // print each instance w/ toString
		System.out.println(pn2.toString());
	}
}