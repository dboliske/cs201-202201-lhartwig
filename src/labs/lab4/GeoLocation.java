package labs.lab4;

public class GeoLocation {
	// Lillian Hartwig, CS201-01, 2/14/22, GeoLocation
	
	private double lat; // attributes; stored variables
	private double lng;
	
	public GeoLocation() { // default constructor
		lat = 0.0;
		lng = 0.0;
	}
	
	public GeoLocation(double lt, double lg) { // non-default constructor
		lat = 0.0;
		setLat(lt);
		lng = 0.0;
		setLng(lg);
	}
	
	public double getLat() { // accessor method for lat
		return lat;
	}
	public double getLng() { // accessor method for lng
		return lng;
	}
	
	public void setLat(double lat) { // mutator for lat
		this.lat = lat;
	}
	
	public void setLng(double lng) { // mutator for lng
		this.lng = lng;
	}
	
	public String toString() { // preset string representing the data: "(lat,lng)"
		return "(" + lat + "," + lng + ")";
	}
	
	public boolean validLat(GeoLocation l) {
		if(l.getLat() > -90 && l.getLat() < 90) { // checks if lat is between -90 and +90
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validLng(GeoLocation l) {
		if(l.getLng() > -180 && l.getLng() < 180) { // checks if lng is between -180 and +180
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(GeoLocation l) { // checks if two locations are equal
		if (this.lat != l.getLat()) {
			return false;
		} if (this.lng != l.getLng()) {
			return false;
		} else {
			return true;
		}
	}
}