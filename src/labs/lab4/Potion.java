package labs.lab4;

public class Potion {
	// Lillian Hartwig, CS201-01, 2/14/22, Potion
	
	private String name;
	private double strength;
	
	public Potion() { // default constructor
		name = "Potion";
		strength = 0.0;
	}
	
	public Potion(String n, double s) { // non-default constructor
		name = "Potion";
		setName(n);
		strength = 0.0;
		setStrength(s);
	}
	
	public String getName() { // accessor for name
		return name;
	}
	
	public double getStrength() { // accessor for strength
		return strength;
	}
	
	public void setName(String name) { // mutator for name
		this.name = name;
	}
	
	public void setStrength(double strength) { // mutator for strength
		this.strength = strength;
	}
	
	public String toString() { // preset string of potion name and strength
		return "Potion name: " + name + "; Strength: " + strength;
	}
	
	public boolean validStrength(Potion p) { // checks that strength is between 0 and 10
		if(p.getStrength() >= 0 && p.getStrength() <= 10) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(Potion p) { // checks if two potions are equal
		if (this.name != p.getName()) {
			return false;
		} if (this.strength != p.getStrength()) {
			return false;
		} else {
			return true;
		}
	}
}