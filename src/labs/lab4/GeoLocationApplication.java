package labs.lab4;

public class GeoLocationApplication {

	public static void main(String[] args) {
		// Lillian Hartwig, CS201-01, 2/16/22, GeoLocationApplication
		// create two instances and print w/ accessor methods
		
		GeoLocation g1 = new GeoLocation(); // create instance w/ default constructor
		GeoLocation g2 = new GeoLocation(-87, 149); // create instance w/ non-default constructor
		
		System.out.println(g1.getLat()); // print values of instance variables w/ accessor methods
		System.out.println(g1.getLng());
		System.out.println(g2.getLat());
		System.out.println(g2.getLng());
	}
}