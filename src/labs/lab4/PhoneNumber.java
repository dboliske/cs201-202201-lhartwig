package labs.lab4;

public class PhoneNumber {
	// Lillian Hartwig, CS201-01, 2/14/22, PhoneNumber
	
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() { // default constructor
		countryCode = "1";
		areaCode = "312";
		number = "1234567";
	}
	
	public PhoneNumber(String country, String area, String num) { // non-default constructor
		countryCode = "1";
		setCountryCode(country);
		areaCode = "312";
		setAreaCode(area);
		number = "1234567";
		setNumber(num);
	}
	
	public String getCountryCode() { // accessor for country code
		return countryCode;
	}
	
	public String getAreaCode() { // accessor for area code
		return areaCode;
	}
	
	public String getNumber() { // accessor for number
		return number;
	}
	
	public void setCountryCode(String countryCode) { // mutator for country code
		this.countryCode = countryCode;
	}
	
	public void setAreaCode(String areaCode) { // mutator for area code
		this.areaCode = areaCode;
	}
	
	public void setNumber(String number) { // mutator for number
		this.number = number;
	}
	
	public String toString() { // preset string representing the phone number
		return "+" + countryCode + " (" + areaCode + ") " + number;
	}
	
	public boolean validAreaCode(PhoneNumber n) { // checks that area code is 3 digits
		if(n.getAreaCode().length() != 3) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean validNumber(PhoneNumber n) { // checks that number is 7 digits
		if(n.getNumber().length() != 7) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean equals(PhoneNumber n) { // checks if two phone numbers are equal
		if (this.countryCode != n.getCountryCode()) {
			return false;
		} if (this.areaCode != n.getAreaCode()) {
			return false;
		} if (this.number != n.getNumber()) {
			return false;
		} else {
			return true;
		}
	}
}