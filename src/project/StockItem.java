package project;

public class StockItem {
	// Lillian Hartwig, CS 201, 4/27/22, StockItem.java
	
	protected String item;
	protected Double price;
	protected Date expiration;
	protected Integer age;
	
	public StockItem() {
		item = "item name";
		price = 0.00;
	}
	
	public StockItem(String item, Double price) {
		item = "item name";
		nameItem(item);
		price = 0.00;
		setPrice(price);
	}
	
	public StockItem(String item, Double price, int y, int m, int d) {
		item = "item name";
		nameItem(item);
		price = 0.00;
		setPrice(price);
		this.expiration =  new Date();
		setExpirationDate(y, m, d);
		
	}
	
	public StockItem(String item, Double price, int age) {
		item = "item name";
		nameItem(item);
		price = 0.00;
		setPrice(price);
		age = 21;
		setAgeRestriction(age);
	}

	public String getItem() {
		return item;
	}

	public void nameItem(String item) {
		this.item = item;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		if (price > 0) {
			this.price = price;
		} else {
			System.out.println("Invalid price entered.");
		}
	}

	public String getExpirationDate() {
		return expiration.toString();
	}

	public void setExpirationDate(int y, int m, int d) {
		if ((new Date(y, m, d)).after(new Date(2022, 4, 29))) {
			this.expiration = new Date(y, m, d);
		} else {
			System.out.println("Invalid expiration date entered.");
		}
	}

	public int getAgeRestriction() {
		return age;
	}

	public void setAgeRestriction(int age) {
		if (age > 0) {
			this.age = age;
		} else {
			System.out.println("Invalid age entered.");
		}
	}
	
	public String toString() {
		return item + ", $" + price;
	}
}