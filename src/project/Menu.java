package project;

import java.util.Scanner;

public class Menu extends Store{
	// Lillian Hartwig, CS 201, 4/27/22, Menu.java
	
	private String input;
	
	public Menu(Scanner input, boolean open) {
		setInput(input);
		switch(this.input) {
		// add item
		case"1":
			Scanner addWhat = new Scanner(System.in);
			System.out.println("Which item type is being added?");
			System.out.println("1. Shelved item");
			System.out.println("2. Produce item");
			System.out.println("3. Age resitricted item");
			System.out.print("Choice: "); String choice = addWhat.nextLine();
			addWhat.close();
			switch(choice) {
			case "1":
				Scanner add = new Scanner(System.in);
				System.out.print("Enter item: "); String addItem = add.nextLine();
				System.out.print("Enter price: "); Double addPrice = Double.parseDouble(add.nextLine());
				AddStock.addShelved(items, itemKinds, addItem, addPrice);
				add.close();
				break;
			case "2":
				Scanner add1 = new Scanner(System.in);
				System.out.print("Enter item: "); String addItem1 = add1.nextLine();
				System.out.print("Enter price: "); Double addPrice1 = Double.parseDouble(add1.nextLine());
				System.out.print("Enter date (yyyy,mm,dd): "); String addYMD = add1.nextLine();
				String[] addDateS = addYMD.split(",");
				int[] addDate = new int[3];
				for (int i  = 0; i < addDateS.length; i++) {
					addDate[i] = Integer.parseInt(addDateS[i]);
				}
				AddStock.addProduce(items, itemKinds, addItem1, addPrice1, addDate[0], addDate[1], addDate[2]);
				add1.close();
				break;
			case "3":
				Scanner add2 = new Scanner(System.in);
				System.out.print("Enter item: "); String addItem2 = add2.nextLine();
				System.out.print("Enter price: "); Double addPrice2 = Double.parseDouble(add2.nextLine());
				System.out.print("Enter age restriction: "); int addAge = Integer.parseInt(add2.nextLine());
				AddStock.addAgeRestricted(items, itemKinds, addItem2, addPrice2, addAge);
				add2.close();
				break;
			default:
				System.out.println("Invalid choice entered.");
			}
			open = true;
			break;
		// search stock
		case "2":
			SearchStock.search(items);
			open = true;
			break;
		// update item
		case "3":
			UpdateStock.update(items);
			open = true;
			break;
		// go to cart
		case "4":
			boolean inCart = true;
			do {
				Scanner cartInput = new Scanner(System.in);
				System.out.println("Menu:");
				System.out.println("1. Add to cart");
				System.out.println("2. Remove from cart");
				System.out.println("3. Check cart");
				System.out.println("4. Return to store"); // exit
				System.out.print("Choice: ");
				new CartMenu(cartInput, inCart);
				cartInput.close();
			} while (inCart);
			open = true;
			break;
		// checkout (exit)
		case "5":
			new Checkout();
			open = false;
			break;
		default:
			System.out.println("Invalid choice entered.");
			open = true;
		}
	}

	public void setInput(Scanner input) {
		this.input = input.nextLine();
	}
}