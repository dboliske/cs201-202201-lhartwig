package project;

import java.util.Scanner;

public class UpdateStock extends Stock {
	// Lillian Hartwig, CS 201, 4/27/22, UpdateStock.java
	
	private static int index;
	
	public static void updateStock(StockItem[] items, String originalItem) {
		try {
			Scanner update = new Scanner(System.in);
			index = SearchStock.itemSearch(items, originalItem);
			System.out.print("Enter new item name: ");
			String newItem = update.nextLine();
			items[index].nameItem(newItem);
			System.out.print("and the new price: ");
			Double newPrice = Double.parseDouble(update.nextLine());
			items[index].setPrice(newPrice);
			update.close();
		} catch (Exception e) {
			System.out.println("Item cannot be found or does not exist.");
		}
	}
	
	public static void update(StockItem[] items) {
		Scanner update = new Scanner(System.in);
		System.out.print("Enter the original item name: ");
		String originalItem = update.nextLine();
		updateStock(items, originalItem);
		update.close();
	}
}