package project;

public class AddStock extends Stock {
	// Lillian Hartwig, CS 201, 4/27/22, AddStock.java
	
	public static void addShelved(StockItem[] items, int itemKinds, String item, Double price) {
		StockItem temp = new StockItem(item, price);
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				items[i] = temp;
			} else {
				resizeItems(items, itemKinds, temp);
			}
		}
	}
	
	public static void addProduce(StockItem[] items, int itemKinds, String item, Double price, int y, int m, int d) {
		StockItem temp = new StockItem(item, price, y, m, d);
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				items[i] = temp;
			} else {
				resizeItems(items, itemKinds, temp);
			}
		}
	}
	
	public static void addAgeRestricted(StockItem[] items, int itemKinds, String item, Double price, int age) {
		StockItem temp = new StockItem(item, price, age);
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				items[i] = temp;
			} else {
				resizeItems(items, itemKinds, temp);
			}
		}
	}
	
	public static void resizeItems(StockItem[] items, int itemKinds, StockItem s) {
		if (itemKinds == items.length) {
			// resize
			StockItem[] temp = new StockItem[items.length * 2];
			for (int i = 0; i < items.length; i++) {
				temp[i] = items[i];
			}
			items = temp;
			temp = null;
		}
		
		items[itemKinds] = s;
		itemKinds++;
	}
}