package project;

public class Date {
	// Lillian Hartwig, CS 201, 4/27/22, Date.java
	
	private int year;
	private int month;
	private int day;
	
	public Date() {
		year = 2022;
		month = 4;
		day = 29;
	}
	
	public Date(int y, int m, int d) {
		year = 2022;
		setYear(y);
		month = 4;
		setMonth(m);
		day = 29;
		setDay(d);
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setMonth(int month) {
		if (month >= 1 && month <= 12) {
			this.month = month;
		}
	}
	
	public void setDay(int day) {
		if (day >= 1 && day <= 31) {
			this.day = day;
		}
	}
	
	public int getYear() {
		return year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}
	
	public String toString() {
		return year + "-" +
			(month<10?("0" + month): month) + "-" +
			(day<10?("0" + day): day); 
	}
	
	public boolean equals(Date d) {
		if (this.year != d.getYear()) {
			return false;
		} else if (this.month != d.getMonth()) {
			return false;
		} else if (this.day != d.getDay()) {
			return false;
		}
		
		return true;
	}
	
	public boolean before(Date d) {
		if (this.year > d.getYear()) {
			return false;
		} else if (this.year == d.getYear()) {
			if (this.month > d.getMonth()){
				return false;
			} else if (this.month == d.getMonth()) {
				if (this.day > d.getDay()) {
					return false;
					}
				}
			}
		
		return true;
	}
	
	public boolean after(Date d) {
		return !equals(d) && !before(d);
	}
}