package project;

import java.util.Scanner;

public class SearchStock extends Stock {
	// Lillian Hartwig, CS 201, 4/27/22, SearchStock.java
	
	public static int itemSearch(StockItem[] items, String item) {
		for (int i = 0; i < items.length; i++) {
			if (items[i].getItem().equalsIgnoreCase(item)) {
				return i;
			}
		}
		return -1;
	}
	
	public static int itemPriceSearch(StockItem[] items, String item, Double price) {
		StockItem[] tempIndex = new StockItem[items.length];
		for (int i = 0; i < tempIndex.length; i++) {
			for (int j = 0; j < items.length; j++) {
				if (items[j].getItem().equalsIgnoreCase(item)) {
					tempIndex[i] = items[j];
				}
			}
			if (tempIndex[i].getPrice() == price) {
				return i;
			}
		}
		return -1;
	}
	
	public static void search(StockItem[] items) {
		Scanner search = new Scanner(System.in);
		System.out.print("Enter item to search for: ");
		String searchItem = search.nextLine();
		System.out.print("Enter item price (optional, enter 0 if price is unknown)");
		Double searchPrice = Double.parseDouble(search.nextLine());
		search.close();
		if (searchPrice != 0) {
			int index = itemPriceSearch(items, searchItem, searchPrice);
			System.out.println(items[index].toString());
		} else {
			int index = itemSearch(items, searchItem);
			System.out.println(items[index].toString());
		}
	}
	
	public static int searchIndex(StockItem[] items) {
		Scanner search = new Scanner(System.in);
		System.out.print("Enter item to search for: ");
		String searchItem = search.nextLine();
		System.out.print("Enter item price (optional, enter 0 if price is unknown)");
		Double searchPrice = Double.parseDouble(search.nextLine());
		search.close();
		if (searchPrice != 0) {
			int index = itemPriceSearch(items, searchItem, searchPrice);
			return index;
		} else {
			int index = itemSearch(items, searchItem);
			return index;
		}
	}
}