package project;

import java.util.Scanner;

public class CartMenu extends Store {
	// Lillian Hartwig, CS 201, 4/27/22, CartMenu.java
	
	private String input;
	public int cartItems = 20;
	public StockItem[] cart = new StockItem[cartItems];
	
	public CartMenu(Scanner input, boolean inCart) {
		// cart menu
		setInput(input);
		
		switch(this.input) {
		// add to cart
		case "1":
			System.out.println("What would you like to add?");
			int addIndex = SearchStock.searchIndex(items);
			Cart.addToCart(cart, cartItems, items, addIndex);
			inCart = true;
			break;
		// remove from cart
		case "2":
			System.out.println("What would you like to remove?");
			int removeIndex = SearchStock.searchIndex(cart);
			Cart.removeFromCart(cart, removeIndex);
			inCart = true;
			break;
		// check cart
		case "3":
			Double total = 0.0;
			for (int i  = 0; i < cartItems; i++) {
				System.out.println(cart[i].toString());
				total = total + cart[i].getPrice();
			}
			System.out.println("Cart total is: $");
			inCart = true;
			break;
		// return to store
		case "4":
			System.out.println("Returning to store...");
			inCart = false;
			break;
		default:
			System.out.println("Invalid choice entered.");
		}
	}

	public String getInput() {
		return input;
	}

	public void setInput(Scanner input) {
		this.input = input.nextLine();
	}
}