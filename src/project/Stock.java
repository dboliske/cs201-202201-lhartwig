package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Stock extends StockItem {
	// Lillian Hartwig, CS 201, 4/27/22, Stock.java
	
	public int itemKinds = 60;
	public StockItem[] items = new StockItem[itemKinds];
	
	public Stock() {
		String[] tempA = new String[itemKinds];
		String[] tempB = new String[3];
		String doubString;
		double doubTemp;
		String[] dateString = new String[3];
		int[] tempDate = new int[3];
		String intString;
		int tempInt;
		File f = new File("src/project/stock.csv");
		try {
			Scanner tempIn = new Scanner(f);
			do {
				Scanner in = new Scanner(f);
				for (int i = 0; i < itemKinds; i++) {
					tempA[i] = in.nextLine();
				}
				in.close();
			} while (tempIn.hasNextLine());
			tempIn.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error! File not found!");
		}
		
		for (int i = 0; i < itemKinds; i++) {
			tempB = tempA[i].split(",");
			doubString = tempB[1];
			doubTemp = Double.parseDouble(doubString);
			if (tempB[3].contains("/")) {
				dateString = tempB[3].split("/");
				for (int j = 0; j < tempDate.length; i++) {
					tempDate[j] = Integer.parseInt(dateString[j]);
				}
				StockItem newItem = new StockItem(tempB[0], doubTemp, tempDate[0], tempDate[1], tempDate[2]);
				items[i] = newItem;
			} if (!(tempB[3].contains("/"))) {
				intString = tempB[3];
				tempInt = Integer.parseInt(intString);
				StockItem newItem = new StockItem(tempB[0], doubTemp, tempInt);
				items[i] = newItem;
			} else {
				tempB[3] = null;
				StockItem newItem = new StockItem(tempB[0], doubTemp);
				items[i] = newItem;
			}
			tempA = null;
			tempB = null;
		}
	}
}