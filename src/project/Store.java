package project;

import java.util.Scanner;

public class Store extends Stock{
	// Lillian Hartwig, CS 201, 4/27/22, Store.java

	public static void main(String[] args) {
		boolean open = true;
		do {
			// menu
			Scanner menuInput = new Scanner(System.in);
			System.out.println("Menu:");
			System.out.println("1. Add Stock");
			System.out.println("2. Search Stock");
			System.out.println("3. Update Stock");
			System.out.println("4. Go to Cart");
			System.out.println("5. Checkout"); // exit
			System.out.print("Choice: ");
			new Menu(menuInput,open);
			menuInput.close();
		} while(open);
		
		System.out.println("Thank you for shopping, have a good day!");
	}
}