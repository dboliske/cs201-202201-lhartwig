package project;

public class Checkout extends Store {
	// Lillian Hartwig, CS 201, 4/27/22, Checkout.java
	
	public Checkout() {
		Double total = 0.0;
		for (int i = 0; i < items.length; i++) {
			System.out.println(items[i].toString());
			total = total + items[i].getPrice();
		}
		System.out.println("Your total is: $" + total);
	}
}