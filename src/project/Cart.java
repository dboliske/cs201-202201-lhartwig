package project;

public class Cart extends StockItem {
	// Lillian Hartwig, CS 201, 4/27/22, Cart.java
	
	public static void addToCart(StockItem[] cart, int cartItems, StockItem[] items, int index) {
		for (int i = 0; i < cart.length; i++) {
			if (cart[i] ==  null) {
				cart[i] = items[index];
			} else {
				//  resize cart
				AddStock.resizeItems(cart, cartItems, items[index]);
			}
		}
	}
	
	public static void removeFromCart(StockItem[] cart, int index) {
		cart[index] = null;
	}
}