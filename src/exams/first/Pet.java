package exams.first;

public class Pet {
	// Lillian Hartwig; Exam 1; 2/25/22
	// Question Five
	
	private String name;
	private int age;
	
	public Pet() {
		name = "Spot";
		age = 2;
	}
	
	public Pet(String name, int age) {
		name = "Spot";
		setName(name);
		age = 2;
		setAge(age);
	}
	
	public void setName(String name) {
		this.name  = name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet p) {
		if (!(this.name.equals(p.getName()))) {
			return false;
		} else if (this.age != p.getAge()) {
			return  false;
		} else  {
			return true;
		}
	}
	
	public  String toString() {
		return name + " is " + age + " years old.";
	}
}