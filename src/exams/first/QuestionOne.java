package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		// Lillian Hartwig; Exam 1; 2/25/22
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a number: ");
		int num  = Integer.parseInt(input.nextLine());
		int newNum =  num + 65;
		System.out.println((char)newNum);
		
		input.close();
	}
}