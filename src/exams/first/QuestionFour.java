package exams.first;

import java.util.Scanner;

public class QuestionFour {

	public static void main(String[] args) {
		// Lillian Hartwig; Exam 1; 2/25/22
		Scanner input = new Scanner(System.in);
		
		String[] array = new String[5];
		for (int i  = 0; i < array.length; i++) {
			System.out.print("Enter a word: ");
			array[i] = input.nextLine();
		}
		
		input.close();
		
		String[] same = new String[5];
		
		for (int i = 0; i < array.length; i++) {
			String temp = array[i];
			for (int j = 1; j  < array.length; j++) {
				if (array[j].equals(temp)) {
					same[i] = temp;
				} else {
					temp = null;
				}
			}
			System.out.println(same[i]);
		}
	}
}