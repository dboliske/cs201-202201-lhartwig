package exams.first;

import java.util.Scanner;

public class QuestionTwo {

	public static void main(String[] args) {
		// Lillian Hartwig; Exam 1; 2/25/22
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a number: ");
		int num =  Integer.parseInt(input.nextLine());
		if (num % 2 == 0 && num % 3  == 0) {
			System.out.println("foobar");
		} else if (num % 2 == 0) {
			System.out.println("foo");
		} else if (num % 3 == 0) {
			System.out.println("bar");
		} else {
			System.out.println(" ");
		}
		
		input.close();
	}
}