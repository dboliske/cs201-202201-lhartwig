package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		// Lillian Hartwig; Exam 1; 2/25/22
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a number: ");
		int size = Integer.parseInt(input.nextLine());
		int row = size;
		int col = 1;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print("* ");
			}
			System.out.print("\n");
			col++;
		}
		
		input.close();
	}
}