package exams.second;

import java.util.Scanner;

public class QuestionThree {
	// Lillian Hartwig, CS201, 4/29/22, QuestionThree

	public static void main(String[] args) {
		// continuously prompt user for numbers until "done" is entered
		boolean done = false;
		
		int numbers = 10;
		int[] list = new int[numbers];
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter an integer or 'done' twice: ");
		do {
			try {
				for (int i = 0; i < numbers; i++) {
					if (numbers == list.length) {
						numbers++;
					}
					list[i] = Integer.parseInt(input.nextLine());
				}
			} catch(Exception e) {
				if (input.nextLine().equalsIgnoreCase("done")) {
					done = true;
					input.close();
				}
			}
		} while (!done);
		
		int max = list[0];
		int min = list[0];
		
		for (int i = 0; i < list.length; i++) {
			int temp = list[i];
			if (temp <= min) {
				min = temp;
			} if (temp >= max) {
				max = temp;
			}
		}
		
		System.out.println("The maximum value is: " + max + " and the minimum is: " + min);
	}
}