package exams.second;

public class Circle extends Polygon {
	// Lillian Hartwig, CS201, 4/29/22, Circle
	
	private double radius;
	
	public Circle() {
		radius = 1.0;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	@Override
	public String toString() {
		return "Circle [radius: " + radius + ", name: " + name + "]";
	}

	public double area() {
		return Math.PI * radius * radius;
	}
	
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}
}