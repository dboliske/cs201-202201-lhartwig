package exams.second;

public class Classroom {
	// Lillian Hartwig, CS201, 4/29/22, Classroom
	
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "Stuart";
		roomNumber = "112J";
		seats = 25;
	}
	
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public void setSeats(int seats) {
		if (seats > 0) {
			this.seats = seats;
		}
	}

	public String getBuilding() {
		return building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public int getSeats() {
		return seats;
	}

	public String toString() {
		return "Class building: " + building + ", room number: " + roomNumber + ", seats: " + seats;
	}
}