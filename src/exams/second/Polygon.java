package exams.second;

public class Polygon {
	// Lillian Hartwig, CS201, 4/29/22, Polygon
	
	protected String name;
	
	public Polygon() {
		name = "polygon";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Polygon [name: " + name + "]";
	}
}