package exams.second;

public class ComputerLab extends Classroom {
	// Lillian Hartwig, CS201, 4/29/22, ComputerLab
	
	private boolean computers;
	
	public ComputerLab() {
		computers = true;
	}

	public boolean isComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}

	@Override
	public String toString() {
		return "Computer Lab [building: " + building + ", room number: " + roomNumber + ". Computers?" + computers +  "]";
	}
}