package exams.second;

public class Rectangle extends Polygon {
	// Lillian Hartwig, CS201, 4/29/22, Rectangle
	
	private double width;
	private double height;
	
	public Rectangle() {
		width = 1.0;
		height = 1.0;
	}
	
	public void setWidth(double width) {
		if (width > 0) {
			this.width = width;
		}
	}
	
	public void setHeight(double height) {
		if (height > 0) {
			this.height = height;
		}
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	@Override
	public String toString() {
		return "Rectangle [width: " + width + ", height: " + height + ", name: " + name + "]";
	}
	
	public double area() {
		return height * width;
	}
	
	public double perimeter() {
		return 2.0 * (height + width);
	}
}