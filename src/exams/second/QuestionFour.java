package exams.second;

public class QuestionFour {
	// Lillian Hartwig, CS201, 4/29/22, QuestionFour
	
	public static String[] selectionSort(String[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			int min = i;
			for (int j = i + 1; j < array.length; j++) {
				if (array[j].compareTo(array[min]) < 0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		return array;
	}

	public static void main(String[] args) {
		// implement Selection Sort Algorithm and print sorted list
		
		String[] list = {"speaker", "poem", "passenger", "tale", "reflection", "leader", 
				"quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		String[] sortedList = selectionSort(list);
		
		for (int i = 0; i < sortedList.length; i++) {
			System.out.println(sortedList[i]);
		}
	}
}