package exams.second;

import java.util.Scanner;

public class QuestionFive {
	// Lillian Hartwig, CS201, 4/29/22, QuestionFive
	
	public static int jumpSearch(double[] array, double value) {
		int step = (int)Math.sqrt(array.length);
		int prev = 0;
		
		while (array[Math.min(step, array.length - 1)] - value < 0) {
			prev = step;
			step += (int)Math.sqrt(array.length);
			if (prev >= array.length) {
				return -1;
			}
		}
		
		while (array[prev] - value < 0) {
			prev++;
			if (prev == Math.min(step, array.length - 1)) {
				return -1;
			}
		}
		
		if (array[prev] == value) {
			return prev;
		}
		return -1;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] list = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input =  new Scanner(System.in);
		System.out.print("Enter a number to search for: ");
		double searchTerm = Double.parseDouble(input.nextLine());
		
		System.out.println("Index of the search term is: " + jumpSearch(list, searchTerm));
		input.close();
	}
}